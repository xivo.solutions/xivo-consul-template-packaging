# The packaging information for consul-template in XiVO

This repository contains the packaging information for [consul-template](https://github.com/hashicorp/consul-template).

To get a new version of consul-template in the XiVO repository, set the desired
version in the `VERSION` file, update the changelog version.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
